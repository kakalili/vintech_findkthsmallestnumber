import sys
import numbers
def FindKthSmallestNumber(arr1, arr2, k):

    result = sys.maxsize * -1
    if not isinstance(k, numbers.Integral):
        return result
    if not isinstance(arr1, list) or not isinstance(arr2, list):
        return result

    k = k - 1
    if k >= len(arr1) + len(arr2):
        return result

    while k > 0:
        mid = (k - 1)//2
        if mid >= len(arr1):
            arr2 = arr2[mid + 1:]
        elif mid >= len(arr2):
            arr1 = arr1[mid + 1:]
        else:
            if arr1[mid] <= arr2[mid]:
                arr1 = arr1[mid + 1:]
            else:
                arr2 = arr2[mid + 1:]

        k -= mid + 1

    if len(arr1) < 1:
        return arr2[0]
    elif len(arr2) < 1:
        return arr1[0]

    return min(arr1[0], arr2[0])


