# vintech_FindKthSmallestNumber

Given 2 sorted arrays of equal sizes and number k
Find optimal solution and write this method (in the language of your choice)
int FindKthSmallestNumber(arr1, arr2, k)
To find k(th) smallest number out of combined array
